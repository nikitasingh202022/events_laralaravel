<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tbl_event', function (Blueprint $table) {
            $table->increments('eventId');
            $table->string('eventTitle');
            $table->text('description');
            $table->json('images')->nullable();
            $table->dateTime('startTime');
            $table->dateTime('endTime');
            $table->string('typeOfEvent');
            $table->date('endDate');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tbl_event');
    }
};
