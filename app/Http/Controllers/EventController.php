<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function storeData(Request $request)
    {
        $request->validate([
            'eventTitle' => 'required',
            'description' => 'required',
            'images.*' => 'mimes:jpg,png,jpeg,gif,svg',
            'startTime' => 'required',
            'endTime' => 'required',
            'typeOfEvent' => 'required',
            'endDate' => 'required'
        ]);
        dd('dknff');
        if ($request->hasFile('images')) {
            $imgData = [];
            foreach ($request->file('images') as $file) {
                $name = $file->getClientOriginalName();
                $file->move(public_path('uploads'), $name);
                $imgData[] = $name;
            }
        }

        $data = new Event;
        $data->eventTitle = $request->input('eventTitle');
        $data->description = $request->input('description');
        $data->images = json_encode($imgData);
        $data->startTime = $request->input('startTime');
        $data->endTime = $request->input('endTime');
        $data->typeOfEvent = $request->input('typeOfEvent');
        $data->endDate = $request->input('endDate');
        $data->save();

        return redirect('/listing')->with('success', 'Event has been created successfully!');
    }
}
