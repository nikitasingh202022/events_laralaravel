<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
    protected $table = "tbl_event";
    protected $fillable = [
        'eventId',
        'eventTitle',
        'description',
        'images',
        'starttime',
        'endtime',
        'tpEvent',
        'endDate'
    ];
    public  $timestamps = false;
}
