<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.4/dist/jquery.slim.min.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>

</head>

<body>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-6 mt-5">
                <center>
                    <h3 style="font-family:carsive;">Events..🎊🎉</h3>
                </center>
                <div class="p-4 border-2 border border-secondary rounded">
                    <form action="{{URL::to('/importFile')}}" method="POST">
                        @csrf
                        <label for="eventTitle">Event Title:</label>
                        <input type="text" id="eventTitle" name="eventTitle" required><br>

                        <label for="description">Description:</label>
                        <textarea id="description" name="description" required></textarea><br>

                        <label for="images">Images:</label>
                        <input type="file" id="images" name="images[]" multiple><br>

                        <label for="startTime">Start Time:</label>
                        <input type="datetime-local" id="startTime" name="startTime" required><br>

                        <label for="endTime">End Time:</label>
                        <input type="datetime-local" id="endTime" name="endTime" required><br>

                        <label for="typeOfEvent">Type of Event:</label>
                        <select name="typeOfEvent" id="typeOfEvent">
                            <option value="Daily"></option>
                            <option value=" Weekdays"></option>
                            <option value="Weekoff "></option>
                        </select>


                        <label for="endDate">End Date:</label>
                        <input type="date" id="endDate" name="endDate" required><br>

                        <input type="submit" value="Submit" name="submit" class="submit">
                    </form>

                </div>
            </div>
        </div>
    </div>

</body>

</html>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(function() {
        // Multiple images preview with JavaScript
        var previewImages = function(input, imgPreviewPlaceholder) {
            if (input.files) {
                var filesAmount = input.files.length;
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(imgPreviewPlaceholder);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        };
        $('#images').on('change', function() {
            previewImages(this, 'div.images-preview-div');
        });
    });



    // $('.eventForm').validate({
    //     rules: {
    //         eventTitle: {
    //             required: true,
    //         },
    //         description: {
    //             required: true,
    //         },
    //         images: {
    //             required: true,
    //         },
    //         starttime: {
    //             required: true,
    //         },
    //         endtime: {
    //             required: true,
    //         },
    //         tpEvent: {
    //             required: true,
    //         },
    //         endDate: {
    //             required: true,
    //         }
    //     },
    //     messages: {
    //         eventTitle: {
    //             required: "Required Event Title",
    //         },
    //         description: {
    //             required: "required Description Field",
    //         },
    //         images: {
    //             required: "Selects Images",
    //         },
    //         starttime: {
    //             required: "Required Start Time",
    //         },
    //         endtime: {
    //             required: "Required Start End",
    //         },
    //         tpEvent: {
    //             required: "Enter Event Time",
    //         },
    //         endDate: {
    //             required: "Required End Date",
    //         }
    //     }
    // });
</script>